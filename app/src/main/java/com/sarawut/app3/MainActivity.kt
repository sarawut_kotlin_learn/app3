package com.sarawut.app3

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sarawut.app3.model.Oil

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.title = "Oil Price"

        val oilList: List<Oil> = listOf(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 37.24),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 38.08),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 38.35),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.84),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapters = ItemAdapter(oilList)
        recyclerView.adapter = adapters

        adapters.setOnItemClickListener(object : ItemAdapter.OnItemClickListeners {
            override fun onItemClick(position: Int) {
                Log.d("Output", oilList[position].toString() + " | " + oilList[position].name)
                Toast.makeText(
                    this@MainActivity,
                    oilList[position].name + " | " + oilList[position].price.toString(),
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    class ItemAdapter(private val oilList: List<Oil>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

        private lateinit var mListener: OnItemClickListeners

        interface OnItemClickListeners {
            fun onItemClick(position: Int)
        }

        fun setOnItemClickListener(listener: OnItemClickListeners) {
            mListener = listener
        }

        class ViewHolder(private val itemView: View, listener: OnItemClickListeners) :
            RecyclerView.ViewHolder(itemView) {
            val price: TextView = itemView.findViewById<TextView>(R.id.price)
            val name: TextView = itemView.findViewById<TextView>(R.id.name)

            init {
                itemView.setOnClickListener {
                    listener.onItemClick(adapterPosition)
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ViewHolder {
            val adapterLayout =
                LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
            return ViewHolder(adapterLayout, mListener)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.price.text = oilList[position].price.toString()
            holder.name.text = oilList[position].name
        }

        override fun getItemCount(): Int {
            return oilList.size
        }

    }

}